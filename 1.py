import psycopg2
from config import host, user, password, db_name

try:
    # соединение с базой
    connection = psycopg2.connect(
        host = host,
        user = user,
        password = password,
        database=db_name
    )

    connection.autocommit =True

    #Курсор
    with connection.cursor() as cursor:
        cursor.execute(
            "SELECT version();"
        )
        print(f"Server version:{cursor.fetchone()}")

except Exception as _ex:
    print("[info] Error while working on stage 1", _ex)
finally:
    if connection:
        connection.close()
        print("[info] connection closed")

